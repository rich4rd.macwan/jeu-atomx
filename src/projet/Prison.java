package projet;


/**
 * Décrivez votre classe Prison ici.
 *
 * @author (votre nom)
 * @version (un numéro de version ou une date)
 */
public class Prison extends Obstacle
{
    private Particule prisonnier;    
    public Particule getPrisonnier()
    {
        return this.prisonnier;
    }
    public void setPrisonnier(Particule prisonnier)
    {
        this.prisonnier = prisonnier;
    }

    public void execute(Particule p)
    {
        p.setActive(false);
        Particule liberable = this.getPrisonnier();
        if(liberable!=null)
        {
            System.out.println("Prison.execute(): liberable.poids = "+ liberable.getPoids());
            liberable.setActive(true);
           // Jeu.getCourant().setParticuleCourante(liberable);
            liberable.setDirection(p.getDirection());
            liberable.avance();
            Jeu.getCourant().executeOnce(liberable);
        }        
        this.setPrisonnier(p);
    }
    
    public Prison(int poids)
    {
        super(poids);
        setPrisonnier(null);// pas de prisonnier à la création
        setType("Prison");
    }

    @Override
    public String toString() {
        return getType() + " de poids " + getPoids();
    }

    public Prison()
    {
        this(Jeu.getCourant().getAleaPoidsObstacle());
    }

}
