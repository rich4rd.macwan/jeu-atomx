package projet;

import javax.imageio.ImageIO;
import javax.sound.midi.SysexMessage;
import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.geom.Line2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashMap;

public class PlateauPane extends JPanel {
    BufferedImage logo;
    public HashMap<CoupleEntiers,GridButton> cells;
    public CoupleEntiers[] activatedCells;

    public static Color accentColor, markerColor;
    boolean inited = false;
    int dim;
    public JFrame parent;
    private HashMap<CoupleEntiers, Obstacle> obstacles;

    public int getDim(){
        return dim;
    }
    private boolean drawTeleport;
    public PlateauPane(JFrame parent){
        super();
        try {
            logo = (ImageIO.read(getClass().getClassLoader().getResource("resources/logo.png")));
        } catch (IOException e) {
            e.printStackTrace();
        }

        accentColor = new Color(178, 235, 190);
        markerColor = new Color(249, 194, 106);
        this.parent = parent;
        cells = new HashMap<>();
        activatedCells = new CoupleEntiers[2];
        for(int i=0;i<activatedCells.length;i++){
            activatedCells[i] = new CoupleEntiers(0,0);
        }

    }

    public void initPlateau(int dim){
        this.dim = dim;
        if(!inited){
            GridButton b=null;

            Border border = BorderFactory.createLineBorder(Color.GRAY);
            Font font = new Font("Monospaced", Font.PLAIN, 18);
            Emplacement e = null;
            for(int i=0;i<dim+2;i++){
                for(int j = 0;j<dim+2;j++){
                    e = new Emplacement(j,i);
                    if(i==0 || i==dim+1){
                        b = new GridButton(e,false,this,markerColor);

                    }else {
                        if(j==0 || j==dim+1){
                            b = new GridButton(e,false,this,markerColor);

                            }
                        else {
                            b = new GridButton(e,true,this,accentColor);
                            //b.setFocusPainted(false);
                            b.setFont(font);
                            b.setBorder(border);
                        }
                    }

                    add(b);
                }
            }
        }

    }

    public void activateBorderCells(int colonne, int ligne) {
        //First deactivate the old ones
        for(int i=0;i<2;i++){
            if(cells.containsKey(activatedCells[i])){
                cells.get(activatedCells[i]).deactivate();
            }
        }

        if(colonne!=0 || ligne!=0){
            //Activate top row cell
            if(ligne<=dim/2)
                activatedCells[0].setI(0);
            else
                activatedCells[0].setI(dim+1);
            activatedCells[0].setJ(colonne);


            activatedCells[1].setI(ligne);
            if(colonne<=dim/2)
                activatedCells[1].setJ(0);
            else
                activatedCells[1].setJ(dim+1);
        }

        //Activate the new ones
       for(int i=0;i<2;i++){
           if(cells.containsKey(activatedCells[i])){
                cells.get(activatedCells[i]).activate();
           }
        }
    }

    public void repaintDynamics() {
        for(int i=0;i<activatedCells.length;i++){
            if(cells.containsKey(activatedCells[i])){
                cells.get(activatedCells[i]).repaint();
            }
        }
    }

    public void animateCell(Emplacement pos, int gridButtonAnimationType){
        CoupleEntiers ce = pos.getCoupleEntiers();
        //System.out.println("PlateauPane.animateCell "+ ce);
        if(cells.containsKey(ce)){
            if(gridButtonAnimationType==GridButton.ANIMTYPE_ATTRAPER){
                cells.get(ce).attraperParticule();
            }
            //Probably not going to be used, but provided for completeness here
            if(gridButtonAnimationType==GridButton.ANIMTYPE_LANCER){
                cells.get(pos.getCoupleEntiers()).lancerParticule();
            }
        }
        repaintDynamics();
    }

    public  void reveal( HashMap<CoupleEntiers, Obstacle> obstacles){
        setObstacles(obstacles);

    }


    private void setObstacles(HashMap<CoupleEntiers, Obstacle> obstacles) {
        this.obstacles = obstacles;
        for(CoupleEntiers ce:obstacles.keySet()){
            if(cells.containsKey(ce)){
                Obstacle o = obstacles.get(ce);
                cells.get(ce).revealObstacle(o,ce);
            }
        }
        drawTeleport = true;
    }

    public void paint(Graphics g){
        //System.out.println("Repaint");
        super.paint(g);
        //g.fillOval(0,0,300,300);
        g.drawImage(logo,(getWidth()-logo.getWidth()),0,null);
        //g.fillOval(0,0,300,300);
        if(drawTeleport){
            Graphics2D g2 = (Graphics2D)g;
            g2.setStroke(new BasicStroke(2));
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                    RenderingHints.VALUE_ANTIALIAS_ON);
            //drawArrowLine(g,0,0,200,200,10,5);
            g.setColor(Color.gray);

            for(CoupleEntiers ce : obstacles.keySet()){
                Obstacle obs = obstacles.get(ce);
                if(obs!=null && obs.getType().equals(Obstacle.getType(3))){
                    //System.out.println(obs + " à " + ce);
                    GridButton cell = cells.get(ce);
                    if(!cell.wasObstacle) {
                        GridButton cellTo = cells.get(((Teleporteur) obs).getDestination().getCoupleEntiers());

                        drawArrowLine(g, cell.getX() + cell.getWidth() / 2, cell.getY() + cell.getWidth() / 2,
                                cellTo.getWidth() / 2 + cellTo.getX(), cellTo.getHeight() / 2 + cellTo.getY(), 10, 5);
                    }
                }
            }
            //Repaint all cells
            for(CoupleEntiers ce: cells.keySet()){
                GridButton cell = cells.get(ce);
                cell.requestRepaint();
            }

        }

    }

    private void drawArrowLine(Graphics g, int x1, int y1, int x2, int y2, int d, int h) {
        int dx = x2 - x1, dy = y2 - y1;
        double D = Math.sqrt(dx*dx + dy*dy);
        double xm = D - d, xn = xm, ym = h, yn = -h, x;
        double sin = dy / D, cos = dx / D;

        x = xm*cos - ym*sin + x1;
        ym = xm*sin + ym*cos + y1;
        xm = x;

        x = xn*cos - yn*sin + x1;
        yn = xn*sin + yn*cos + y1;
        xn = x;

        int[] xpoints = {x2, (int) xm, (int) xn};
        int[] ypoints = {y2, (int) ym, (int) yn};
        g.setColor(PlateauPane.markerColor);
        g.drawLine(x1, y1, x2, y2);
        g.fillPolygon(xpoints, ypoints, 3);
    }

    public void reinit() {
        for(CoupleEntiers ce:cells.keySet()){
            cells.get(ce).hideObstacle();
        }
    }
}


