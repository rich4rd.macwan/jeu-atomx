package projet;


/**
 * Décrivez votre classe Joueur ici.
 *
 * @author (votre nom)
 * @version (un numéro de version ou une date)
 */
public class Joueur
{
    private int credit;
    private String pseudo="joueur1";
    private int poidsDeParticuleALancer;

    public int getCredit()
    {
        return this.credit;
    }

    public void setCredit(int credit)
    {
        this.credit = credit;
    }
    public void incCredit(int ajout){
        this.setCredit(this.getCredit()+ajout);
    }
    public void decCredit(int retrait){
        this.setCredit(this.getCredit()-retrait);
    }

    public String getPseudo()
    {
        return this.pseudo;
    }

    public void setPseudo(String pseudo)
    {
        this.pseudo = pseudo;
    }

    public int getPoidsDeParticuleALancer(){
        return (poidsDeParticuleALancer<credit)?poidsDeParticuleALancer:credit;
    }
    public void setPoidsDeParticuleALancer(int val){
        poidsDeParticuleALancer = val;
    }
    public Particule getParticuleLancee()
    {
        boolean valide;
        Emplacement e;
        //TODO: All this in the UI
        do
        {
            System.out.println("Entrez les coordonées d'une case du bord du jeu :\n");
            e = new Emplacement(Lire.i("Entrez un numéro de colonne"),Lire.i("Entrez un numéro de ligne"));
            valide = e.isAuBord();
            if(!valide)System.out.println("La case choisie n'est pas une case du bord du jeu.\n");
        }while(!valide);
        
        int poids;int credit=getCredit();
        do
        {
            valide=true;
            poids = Lire.i("Entrez un poids inférieur à "+credit);
            valide = poids>=0 && poids <=credit;
        }
        while(!valide);
        return new Particule(e,e.getDir(),poids);
    }

    public Emplacement getHypothese(int obstacleType, Emplacement e){
        Obstacle o=Jeu.getCourant().getPlateau(e);
        String typeChoisi = Obstacle.getType(obstacleType);
        if(o==null)
        {
            System.out.println("Pas d'obstacle à cet endroit !");
            e=null;
        }
        else
        {
            if(!typeChoisi.equals(o.getType()))
            {
                System.out.println("L'obstacle situé à l'emplacement "+e+" n'a pas le type "+typeChoisi);
                e=null;
            }
        }
        return  e;
    }
    public Emplacement getHypothese()
    {
        System.out.println("Choisissez une position.");
        Emplacement e=new Emplacement(Lire.i("Colonne"),Lire.i("Ligne"));
        Obstacle o=Jeu.getCourant().getPlateau(e);
        if(o==null)
        {
            System.out.println("Pas d'obstacle à cet endroit !");
            e=null;
        }
        else
        {
            for(int i=1;i<=Obstacle.getNbTypes();i++)
            {
                System.out.println(i+"..........."+Obstacle.getType(i));
            }
            int choixNumType;boolean valide;
            do
            {
                choixNumType = Lire.i("Choisissez le numéro d'un type ");
                valide = (choixNumType>=1&&choixNumType<=Obstacle.getNbTypes());
                if(!valide)System.out.println("Choix invalide. Recommencez !\n");
            }while(!valide);
            String typeChoisi = Obstacle.getType(choixNumType);
            if(!typeChoisi.equals(o.getType()))
            {
                System.out.println("L'obstacle situé à l'emplacement "+e+" n'a pas le type "+typeChoisi);
                e=null;
            }
        }
        return e;
    }
    public String toString(){return this.getPseudo()+" : "+this.getCredit()+" points.";}
}
