package projet;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class GridButton extends JComponent {
    public static ConfirmActionDialog confirmDialog;
    public static GridButton previousButton;
    boolean isRestartButton = false;
    PlateauPane plateauPane;
    BufferedImage prisonIcon,deviationIcon, deviationIcon1,deviationIcon2,deviationIcon3,teleportIcon,currentIcon, particleIcon, restartIcon;
    int iconh,iconw, particleIconH,particleIconW;
    boolean wasObstacle;
    int pad = 4;
    int thickness =1;
    private int iconIndex = 3;
    String text;
    boolean isDrawBorderEnabled = false;
    boolean animating, animatingAttraper;
    int animationFrames = 20;
    int animFrameIndex = 0;
    int animDx, animDy;
    Emplacement pos;
    boolean isTile;
    boolean topLabelRow, bottomLabelRow, leftLabelRow,rightLabelRow;
    Font smallFont = new Font("Helvetica", Font.PLAIN, 18);
    Font particleFont = new Font("Monospaced",Font.BOLD,16);
    int fontX,fontY,fontH,fontW;
    int[] triangleX;
    int[] triangleY;

    //Coordinates for the text of particle weights
    Particule currentParticle = null;
    int[] particleWeightStrCoords;
    //Coordinates for the particles
    int[] particleWeightCoords;
    //For animated particles, the coords of the text and the particles
    int[] animParticleStrCoords;
    int[] animParticleCoords;

    int poidsParticule;
    int particleWtFontW,particleWtFontH;
    Color particleColor, lancerColor,attraperColor;
    int currentAnimType= 0;//0 for lancerParticule, 1 for attraperParticule
    public boolean showingObstacle = false;

    public static final int ANIMTYPE_LANCER=0, ANIMTYPE_ATTRAPER=1;
    int triangleH,triangleW;
    String label;
    boolean inited =false;
    boolean activated = false;
    boolean isCorner = false;
    boolean repaintRequested = false;
    public void requestRepaint(){
        if(!repaintRequested) {
            repaintRequested = true;
            repaint();
        }

    }

    public Emplacement getEmplacement(){
        return pos;
    }

    public void paintComponent(Graphics g){
        Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
                RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);

        if(!inited){
            //Init for graphics
            FontMetrics fm = g.getFontMetrics(smallFont);
            int fontW = fm.stringWidth("0");
            int fontH =  fm.getHeight();

            //TODO: Use W/S keys to change these values

            particleWeightStrCoords = new int[2];
            particleWeightCoords = new int[2];
            animParticleCoords = new int[2];
            animParticleStrCoords = new int[2];

            lancerColor = new Color(227,147,40);
            attraperColor = new Color(32,176,164);
            //Marker coordinates for 4 walls
            triangleX = new int[6];                         triangleY = new int[6];
            triangleH= 32; triangleW = 32;
            if(topLabelRow) {
                triangleX[0] = getWidth()/2;                triangleY[0] = getHeight();
                triangleX[1]=(getWidth()-triangleW)/2;      triangleY[1] = getHeight() - triangleH/4;
                triangleX[2]=(getWidth()-triangleW)/2;      triangleY[2] = getHeight() - triangleH;
                triangleX[3] =(getWidth()+triangleW)/2;     triangleY[3] = getHeight() - triangleH;
                triangleX[4] =(getWidth()+triangleW)/2;     triangleY[4] = getHeight() - triangleH/4-1;
                triangleX[5] =getWidth()/2;                 triangleY[5] = getHeight();
                particleWeightCoords[0] = getWidth()/4;
                particleWeightCoords[1] = getHeight()/10;

                particleWeightStrCoords[0] = particleWeightCoords[0]*2 - fontW ;
                particleWeightStrCoords[1] = particleWeightCoords[1]+fontH-3;

                animParticleStrCoords[0] = particleWeightStrCoords[0];
                animParticleStrCoords[1] = particleWeightStrCoords[1];
                animParticleCoords[0] = particleWeightCoords[0];
                animParticleCoords[1] = particleWeightCoords[1];

            }
            if(bottomLabelRow) {
                triangleX[0] = getWidth()/2;                triangleY[0] = 0;
                triangleX[1]=(getWidth()-triangleW)/2;      triangleY[1] = triangleH/3;
                triangleX[2]=(getWidth()-triangleW)/2;      triangleY[2] = triangleH;
                triangleX[3] =(getWidth()+triangleW)/2;     triangleY[3] = triangleH;
                triangleX[4] =(getWidth()+triangleW)/2;     triangleY[4] = triangleH/3;
                triangleX[5] = getWidth()/2;                triangleY[5] = 0;

                particleWeightCoords[0] = getWidth()/4;
                particleWeightCoords[1] = getHeight()/2-5;

                particleWeightStrCoords[0] = particleWeightCoords[0]*2 - fontW ;
                particleWeightStrCoords[1] = particleWeightCoords[1]+fontH-3;


                animParticleStrCoords[0] = particleWeightStrCoords[0];
                animParticleStrCoords[1] = particleWeightStrCoords[1];
                animParticleCoords[0] = particleWeightCoords[0];
                animParticleCoords[1] = particleWeightCoords[1];
            }
            if(leftLabelRow) {
                triangleX[0] = getWidth();                  triangleY[0] = getHeight()/2;
                triangleX[1]=(getWidth()-triangleW/4);      triangleY[1] = (getHeight() - triangleH)/2;
                triangleX[2]=(getWidth()-triangleW);        triangleY[2] = (getHeight() - triangleH)/2;
                triangleX[3] =(getWidth()-triangleW);       triangleY[3] = (getHeight() + triangleH)/2;
                triangleX[4] =(getWidth()-triangleW/4);   triangleY[4] = (getHeight() + triangleH)/2;
                triangleX[5] = getWidth();                  triangleY[5] = getHeight()/2;

                particleWeightCoords[0] = getWidth()/2-fontW*2-3;
                particleWeightCoords[1] = getHeight()/2-fontH/2-3;
                particleWeightStrCoords[0] = particleWeightCoords[0] +fontW/2 +1;
                particleWeightStrCoords[1] = particleWeightCoords[1] + fontH - 3 ;


                animParticleStrCoords[0] = particleWeightStrCoords[0];
                animParticleStrCoords[1] = particleWeightStrCoords[1];
                animParticleCoords[0] = particleWeightCoords[0];
                animParticleCoords[1] = particleWeightCoords[1];

            }
            if(rightLabelRow) {
                triangleX[0] = 0;                           triangleY[0] = getHeight()/2;
                triangleX[1] = triangleW/4;                   triangleY[1] = (getHeight() - triangleH)/2;
                triangleX[2] = triangleW;                   triangleY[2] = (getHeight() - triangleH)/2;
                triangleX[3] = triangleW;                   triangleY[3] = (getHeight() + triangleH)/2;
                triangleX[4] = triangleW/4;                   triangleY[4] = (getHeight() + triangleH)/2;
                triangleX[5] = 0;                           triangleY[5] = getHeight()/2;

                particleWeightCoords[0] = getWidth()/2-fontW;
                particleWeightCoords[1] = getHeight()/2-fontH/2-3;
                particleWeightStrCoords[0] = particleWeightCoords[0] +fontW/2 +1;
                particleWeightStrCoords[1] = particleWeightCoords[1] + fontH - 3 ;

                animParticleStrCoords[0] = particleWeightStrCoords[0];
                animParticleStrCoords[1] = particleWeightStrCoords[1];
                animParticleCoords[0] = particleWeightCoords[0];
                animParticleCoords[1] = particleWeightCoords[1];
            }



            g.setColor(Color.gray);

            int pad=10;
            g.setFont(smallFont);
            if(topLabelRow) {
                int col = pos.getColonne();
                label = "" + col;

                if(col<10){
                    fontX = getWidth()/2-fontW/2; fontY = getHeight() - pad;
                }else{
                    fontX = getWidth()/2-fontW; fontY = getHeight() -pad;
                }
                //g.drawString(""+pos.getColonne(),getWidth()/2,getHeight()-5);
            }
            else if(bottomLabelRow) {
                int col = pos.getColonne();
                label = "" + col;
                if(col<10){
                    fontX = getWidth()/2-fontW/2; fontY = fontH+pad/2-4;
                }else{
                    fontX = getWidth()/2-fontW; fontY = fontH+pad/2-4;
                }
                //g.drawString(""+pos.getColonne(),getWidth()/2,20);
            }
            else if(leftLabelRow){
                int ligne = pos.getLigne();
                label = "" + ligne;
                if(ligne<10) {
                    fontX = getWidth() - fontW-pad; fontY = (getHeight())/2+pad/2+3;
                }else{
                    fontX = getWidth() - fontW-pad*2; fontY = (getHeight())/2+pad/2+3;
                }
                //int strw = 25; int strh = 5;
                //g.drawString(""+pos.getLigne(),getWidth()-strw,getHeight()/2+strh);
            }
            else if(rightLabelRow){
                //int strw = 25; int strh = 5;
                int ligne = pos.getLigne();
                label = "" + ligne;
                fontX = pad/2; fontY = (getHeight())/2+pad/2+3;
                //g.drawString(""+pos.getLigne(),5,getHeight()/2+strh);
            }
            animating = false;
            inited = true;
        }

        if(!isCorner){

            g.setFont(smallFont);
            if(currentIcon!=null) {

                g.setColor(getBackground());
                g.fillRect(0,0,getWidth(),getHeight());
                //g.drawImage(currentIcon,0,0,currentIcon.getWidth(),currentIcon.getHeight(),null);
                g.drawImage(currentIcon,(getWidth()-iconw)/2,(getHeight()-iconw)/2,iconw,iconh,null);

            }
            else if(!isTile){
                /*g2d.setPaint(Color.blue);
                AlphaComposite alcom = AlphaComposite.getInstance(
                        AlphaComposite.SRC_OVER, .2f);
                g2d.setComposite(alcom);
                g2d.fillRect(50 , 20, 40, 40);
                */
                //g.setColor(new Color(255,200,200,10));
                //g.fillRect(0,0,getWidth(),getHeight());
                if (activated){
                    //Draw triangles
                    //g.fillRect(0, 0, getWidth() , getHeight() );
                    g.setColor(Color.gray);
                    g.drawPolygon(triangleX,triangleY,5);
                    g.setColor(getBackground());
                    g.fillPolygon(triangleX,triangleY,5);
                    g.setFont(particleFont);
                    g.setColor(Color.lightGray);
                    g.fillOval(particleWeightCoords[0],particleWeightCoords[1],getWidth()/2,getHeight()/2);

                    //g.drawString("20",particleWeightStrCoords[0],particleWeightStrCoords[1]);
                    g.setColor(Color.black);
                    g.drawOval(particleWeightCoords[0],particleWeightCoords[1],getWidth()/2,getHeight()/2);
                    poidsParticule = Jeu.getCourant().getJoueurCourant().getPoidsDeParticuleALancer();
                    if(poidsParticule<10){
                        g.drawString(""+poidsParticule,particleWeightStrCoords[0]+fontW,particleWeightStrCoords[1]);
                    }
                    else{
                        g.drawString(""+poidsParticule,particleWeightStrCoords[0],particleWeightStrCoords[1]);
                    }
                    //g.drawString("20",particleWeightStrCoords[0],particleWeightStrCoords[1]);
                    //g.drawImage(particleIcon,(getWidth()-particleIconW)/2,(getHeight()-particleIconH)/2-8,null);
                }
                else{
                    if(isDrawBorderEnabled) {
                        g.setColor(getBackground());
                        g.fillRect(0, 0, getWidth() , getHeight() );
                    }
                    g.setColor(Color.GRAY);

                }
                if(animating){
                    if(animFrameIndex<animationFrames) {
                        g.setFont(particleFont);
                        g.setColor(particleColor);
                        g.fillOval(animParticleCoords[0] + animFrameIndex * animDx, animParticleCoords[1] + animFrameIndex * animDy, getWidth() / 2, getHeight() / 2);
                        g.setColor(Color.black);
                        poidsParticule = Jeu.getCourant().getJoueurCourant().getPoidsDeParticuleALancer();
                        if(poidsParticule<10){
                            g.drawString(""+poidsParticule,animParticleStrCoords[0]+ animFrameIndex * animDx+fontW,animParticleStrCoords[1]+ animFrameIndex * animDy);
                        }
                        else{
                            g.drawString(""+poidsParticule,animParticleStrCoords[0]+ animFrameIndex * animDx,animParticleStrCoords[1]+ animFrameIndex * animDy);
                        }
                        ++animFrameIndex;
                    }
                    else{
                        animating = false;
                        animFrameIndex = 0;
                       // lancerParticuleEnJeu(false);
                    }
                }

                g.setColor(Color.DARK_GRAY);
                g.setFont(smallFont);
                g.drawString(label,fontX,fontY);
                //g.drawString(label,10,40);

            }
            else{
                if(!wasObstacle) {
                //g.drawRect(0, 0, getWidth() , getHeight());
                    //Mouse over
                    if(isDrawBorderEnabled) {
                        g.setColor(getBackground());
                    }else {
                        g.setColor(Color.white);
                    }
                    g.fillRect(0, 0, getWidth() , getHeight() );
                    g.setColor(Color.GRAY);
                    g.setFont(smallFont);
                }
            }
                //Was an obstacle, draw disabled
            g.drawImage(currentIcon,(getWidth()-iconw)/2,(getHeight()-iconw)/2,iconw,iconh,null);

        }else{
            if(isRestartButton){
                //Draw the restart icon
                if(isDrawBorderEnabled){
                    g.drawImage(restartIcon,(getWidth()-restartIcon.getWidth())/2,(getHeight()-restartIcon.getHeight())/2,null);
                }
            }
        }

       // g.drawString(pos.getLigne()+","+pos.getColonne(),20,fontH);
    }
    public GridButton(String text){
        //super(text);
        this.text = text;
    }

    private void updateIcon() {
        //iconIndex is obstacleType(int) + 1. See Obstacle.java
        switch (iconIndex){
            case 0:
                currentIcon = prisonIcon;
                break;
            case 1:
                currentIcon = deviationIcon;
                break;
            case 2:
                currentIcon = teleportIcon;
                break;
            default:
                currentIcon = null;
                //t=text;
                break;
        }
        repaint();
    }


    public GridButton(Emplacement e, boolean isTile, PlateauPane p, Color bgcolor)
    {
        text = e.getCoupleEntiers().toString();
        this.isTile = isTile;
        setBackground(bgcolor);
        wasObstacle = false;
        //Add this button to the gridbuttons hashmap, used for activating the border cells based on mouse position
        this.plateauPane = p;
        this.pos = e;
        plateauPane.cells.put(e.getCoupleEntiers(),this);
        int dim = p.getDim()+2;

        if( (e.getLigne()==0 && e.getColonne()==0) || (e.getLigne()==dim-1 && e.getColonne()==dim-1)
        || (e.getLigne()==0 && e.getColonne()==dim-1) || (e.getLigne()==dim-1 && e.getColonne()==0)){
            isCorner = true;
            if((e.getLigne()==0 && e.getColonne()==0)){
                isRestartButton = true;
                setToolTipText("Restart Game");
            }
        }
        label = "";
        fontX = 0; fontY = 0;
        fontW = 5; fontH=20;
        //Init images and set up mouseclick listener
        try {
            prisonIcon = (ImageIO.read(getClass().getClassLoader().getResource("resources/prison.png")));
            deviationIcon =(ImageIO.read(getClass().getClassLoader().getResource("resources/deviation.png")));
            deviationIcon1 =(ImageIO.read(getClass().getClassLoader().getResource("resources/rotate90.png")));
            deviationIcon2 =(ImageIO.read(getClass().getClassLoader().getResource("resources/rotate180.png")));
            deviationIcon3 =(ImageIO.read(getClass().getClassLoader().getResource("resources/rotate270.png")));

            teleportIcon = (ImageIO.read(getClass().getClassLoader().getResource("resources/teleport.png")));
            particleIcon = (ImageIO.read(getClass().getClassLoader().getResource("resources/particle.png")));
            restartIcon = (ImageIO.read(getClass().getClassLoader().getResource("resources/restart.png")));
            currentIcon = prisonIcon;
            iconh = currentIcon.getHeight();
            iconw = currentIcon.getWidth();
            particleIconH = particleIcon.getHeight();
            particleIconW = particleIcon.getWidth();

        }catch(IOException ioe){ioe.printStackTrace();}
        addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if(SwingUtilities.isRightMouseButton(e)) {
                    //if(!isTile) {attraperParticule();}
                }else{
                    if(isRestartButton){
                        JButton yesbtn = new JButton("Yes");
                        yesbtn.setBackground(plateauPane.markerColor);
                        yesbtn.addActionListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent actionEvent) {
                                EventQueue.invokeLater(new Runnable()
                                {
                                    public void run()
                                    {
                                        Jeu.newGame();
                                    }
                                });
                                JOptionPane.getRootFrame().dispose();
                            }
                        });

                        JButton nobtn = new JButton("No");
                        nobtn.setBackground(plateauPane.markerColor);
                        nobtn.addActionListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent actionEvent) {
                                JOptionPane.getRootFrame().dispose();
                            }
                        });

                        JButton[] buttons = { yesbtn, nobtn};
                        try {
                            JOptionPane.showOptionDialog(null, "Restart game?", "Confirmation",
                                    JOptionPane.OK_OPTION, JOptionPane.QUESTION_MESSAGE,
                                    new ImageIcon(ImageIO.read(getClass().getClassLoader().getResource("resources/yesno.png"))),
                                    buttons, buttons[0]);
                        } catch (IOException e1) {
                            e1.printStackTrace();
                        }
                        //int input = JOptionPane.showConfirmDialog(null, "Restart game?","Confirmation",JOptionPane.YES_NO_OPTION);
                        // 0=yes, 1=no, 2=cancel
                        //if(input == 0) {
                        //   Jeu.newGame();
                        //}
                    }
                    else{
                        if(isTile) {
                            if(!previousButton.wasObstacle) {
                                previousButton.currentIcon = null;
                                previousButton.repaint();
                                GridButton source = (GridButton) e.getSource();
                                iconIndex = (++iconIndex) % 3;
                                Jeu.setMessage("Confirmer l'obstacle.");
                                ConfirmActionDialog.invoke(source, (pos.getColonne()) * getWidth(), (pos.getLigne() + 1) * getHeight(), getWidth());
                                updateIcon();
                                previousButton = source;
                            }else{
                                GridButton source = (GridButton) e.getSource();
                                iconIndex = (++iconIndex) % 3;
                                Jeu.setMessage("Confirmer l'obstacle.");
                                ConfirmActionDialog.invoke(source, (pos.getColonne()) * getWidth(), (pos.getLigne() + 1) * getHeight(), getWidth());
                                updateIcon();
                                previousButton = source;
                            }
                        }
                        else{
                            if(!isTile){
                                //Animate lancer de particule
                                lancerParticule();
                            }
                        }
                    }
                }


            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {
                isDrawBorderEnabled = true;
                activated = true;
                plateauPane.activateBorderCells(pos.getLigne(),pos.getColonne());

                //System.out.println("Entered : "+pos.getLigne()+","+pos.getColonne());
                repaint();
            }

            @Override
            public void mouseExited(MouseEvent e) {
                isDrawBorderEnabled = false;
                activated = false;
                //System.out.println("Exited : "+pos.getLigne()+","+pos.getColonne());
                repaint();
            }
        });

        //System.out.println(e + " : isAuBord : " + e.isAuBord()+" ; colonne=" + e.getColonne() );

        topLabelRow = pos.getLigne()==0  && (pos.getColonne()!=0 && pos.getColonne()!=dim-1 );
        bottomLabelRow = pos.getLigne()==dim-1 && (pos.getColonne()!=0 && pos.getColonne()!=dim-1 );
        leftLabelRow = pos.getColonne()==0 && (pos.getLigne()!=0 && pos.getLigne()!=dim-1 );
        rightLabelRow = pos.getColonne()==dim-1&& (pos.getLigne()!=-1 && pos.getLigne()!=dim-1 );
        setDoubleBuffered(true);
        updateIcon();
        previousButton = this;

    }


    public void deactivate() {
        activated = false;
        //System.out.println(pos.getCoupleEntiers().toString() + " activated : " + activated);
        repaint();
    }

    public void activate() {
        activated = true;
        //System.out.println(pos.getCoupleEntiers().toString() + " activated : " + activated);
        repaint();
    }

    public boolean validateObstacle( Emplacement echoisi) {
        boolean result=false;
        //TODO: Check whether the chosen obstacle (by using iconIndex) is correct based on existing obstacles on this plateau
        // Is yes, then lock the obstacle, show a confirmed icon on top left of the obstacle. Update credit
        // If no, show indication that it was wrong and update( remove points) credit, reset obstacle to empty

        //System.out.println("ValidateObstacle " + Obstacle.getType(iconIndex+1));
        Emplacement e = Jeu.getCourant().getJoueurCourant().getHypothese(iconIndex+1,echoisi);
        String message = "";
        if(e!=null)
        {
            wasObstacle = true;
            Jeu.getCourant().getJoueurCourant().incCredit(Jeu.getCourant().getPlateau(e).getPoids());
            Jeu.getCourant().setPlateau(null, e);
            Jeu.getCourant().decNbObstacles();
            message = "Succ\u00e8s!";
            result = true;

        }
        else{
            message = "Pas la bonne obstacle! R\u00e9essayez.";
        }
        Jeu.setMessage(message);
        Jeu.updateScore();
        updateIcon();
        if(Jeu.getCourant().getNbObstacles()==0){
            GameEnd.invoke(true);
        }
        return result;
    }

    public void lancerParticule(){
        int speed = 4;
        //System.out.println("GridButton.lancerParticule() => " + pos);
        Jeu.setMessage("");
        lancerParticuleEnJeu(true);
        currentAnimType = ANIMTYPE_LANCER;
        particleColor = lancerColor;
        for(int i=0;i<2;i++) {
            animParticleCoords[i] = particleWeightCoords[i];
            animParticleStrCoords[i] = particleWeightStrCoords[i];
        }
        if(topLabelRow){
            animDx = 0; animDy = speed;
        }
        if(leftLabelRow){
            animDx = speed; animDy = 0;
        }
        if(bottomLabelRow){
            animDx = 0; animDy = -speed;
        }
        if(rightLabelRow){
            animDx = -speed; animDy = 0;
        }
        animating = true;
        new Thread(){
            public void run(){
                while(animating) {
                   try {
                        //System.out.println("animating : " + animFrameIndex);
                        Thread.sleep(30);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    repaint();
                }
                //System.out.println("End of lancer animation");
                lancerParticuleEnJeu(false);
                //  System.out.println("Animation finished");
            }
        }.start();
    }

    public void attraperParticule(){
        //System.out.println("GridButton.attraperParticule() => " + pos);
        int speed = 2;
        currentAnimType = ANIMTYPE_ATTRAPER;
        particleColor = attraperColor;
        if(topLabelRow){
            animParticleCoords[0] = particleWeightCoords[0];
            animParticleCoords[1] = particleWeightCoords[1]+getHeight()/2;
            animParticleStrCoords[0] = particleWeightStrCoords[0];
            animParticleStrCoords[1] = particleWeightStrCoords[1]+getHeight()/2-1;
            animDx = 0; animDy = -speed;
        }
        if(bottomLabelRow) {
            animParticleCoords[0] = particleWeightCoords[0];
            animParticleCoords[1] = particleWeightCoords[1]-getHeight()/2;
            animParticleStrCoords[0] = particleWeightStrCoords[0];
            animParticleStrCoords[1] = particleWeightStrCoords[1]-getHeight()/2-2;
            animDx = 0; animDy = speed;
        }
        if(leftLabelRow){
            animParticleCoords[0] = particleWeightCoords[0]+getWidth()/2;
            animParticleCoords[1] = particleWeightCoords[1];
            animParticleStrCoords[0] = particleWeightStrCoords[0]+getWidth()/2;
            animParticleStrCoords[1] = particleWeightStrCoords[1];
            animDx = -speed; animDy = 0;
        }
        if(rightLabelRow){
            animParticleCoords[0] = particleWeightCoords[0]-getWidth()/2;
            animParticleCoords[1] = particleWeightCoords[1];
            animParticleStrCoords[0] = particleWeightStrCoords[0]-getWidth()/2;
            animParticleStrCoords[1] = particleWeightStrCoords[1];
            animDx = speed; animDy = 0;
        }
        animating = true;
        new Thread(){
            public void run(){
                while(animating) {
                    try {
                        //System.out.println("animating : " + animFrameIndex);
                        Thread.sleep(30);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    repaint();
                }

            //System.out.println("Animation finished");
            }
        }.start();


    }

    public void lancerParticuleEnJeu(boolean beforeAnimation){
            Emplacement em = null;
            Direction d = null;
            //Perform lancerParticule in the jeu
            if (leftLabelRow) {
                em = new Emplacement(pos.getColonne() + 1, pos.getLigne());
                d = new Direction(1, 0);
                currentParticle = new Particule(em, d, Jeu.getCourant().getJoueurCourant().getPoidsDeParticuleALancer());
            }
            if (rightLabelRow) {
                em = new Emplacement(pos.getColonne() - 1, pos.getLigne());
                d = new Direction(-1, 0);
                currentParticle = new Particule(em, d, Jeu.getCourant().getJoueurCourant().getPoidsDeParticuleALancer());
            }
            if (topLabelRow) {
                em = new Emplacement(pos.getColonne(), pos.getLigne() + 1);
                d = new Direction(0, 1);
                currentParticle = new Particule(em, d, Jeu.getCourant().getJoueurCourant().getPoidsDeParticuleALancer());
            }
            if (bottomLabelRow) {
                em = new Emplacement(pos.getColonne(), pos.getLigne() - 1);
                d = new Direction(0, -1);
                currentParticle = new Particule(em, d, Jeu.getCourant().getJoueurCourant().getPoidsDeParticuleALancer());
            }
        if(beforeAnimation) {
            Jeu.getCourant().startExecuteOnce(currentParticle);
        }else {
            Jeu.getCourant().executeOnce(currentParticle);
        }
    }

    public void hideObstacle(){
        currentIcon = null;
    }
    public void revealObstacle(Obstacle o, CoupleEntiers ce) {
        if(o.getType().equals(Obstacle.getType(1))){
            //Prison
            currentIcon = prisonIcon;
        }
        if(o.getType().equals(Obstacle.getType(2))){
            //Deviation
            int q = ((Deviateur)o).getNbQuarts();

            if(q==1) currentIcon = deviationIcon1;
            if(q==2) currentIcon = deviationIcon2;
            if(q==3) currentIcon = deviationIcon3;

        }
        if(o.getType().equals(Obstacle.getType(3))){
            //Teleporteur
            currentIcon = teleportIcon;
        }
        showingObstacle = true;
        repaint();
    }
}

