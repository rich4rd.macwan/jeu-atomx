/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet;

/**
 *
 * @author jo
 */
public class Deviateur extends Obstacle
{
    private int nbQuart;
    public int getNbQuarts()
    {
        return nbQuart;
    }
    public void setNbQuarts(int nbQuart)
    {
        this.nbQuart = nbQuart;
    }
    public void execute(Particule a)
    {
        a.setDirection(a.getDirection().getRotation(nbQuart));
    }

    @Override
    public String toString() {
        return getType() + " de "+ nbQuart+ " quarts et de poids " + getPoids() ;
    }

    public Deviateur(int poids,int nbQuarts)
    {
        super(poids);
        setNbQuarts(nbQuarts);
        setType("Déviateur");
    }
    public Deviateur()
    {
        this(Jeu.getCourant().getAleaPoidsObstacle(),1+(int)(Math.random()*3));
    }
    
}
