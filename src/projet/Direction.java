/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet;

/**
 *
 * @author jo
 */
public class Direction
{

    private final CoupleEntiers couple;

    /*
    * la direction ne changera jamais mais pourra générer des direction issues
    * d'elle-même par rotation (Même logique que le toString)
     */

    public int getDirH()
    {
        return couple.getI();
    }

    private void setDirH(int dirH)
    {
        couple.setI(dirH);
    }

    int getDirV()
    {
        return couple.getJ();
    }

    private void setDirV(int dirV)
    {
        couple.setI(dirV);
    }

    public boolean isValide()
    {
        int h = getDirH(), v = getDirV();
        return (h >= -1 && h <= 1) && (v >= -1 && v <= 1);
    }

    public Direction(CoupleEntiers c)
    {
        this.couple = c;
    }

    public Direction(int dirH, int dirV)
    {
        this(new CoupleEntiers(dirH, dirV));
    }

    public Direction(Direction d)
    {
        this(new CoupleEntiers(d.getDirH(), d.getDirV()));
    }

    public Direction()
    {
        this(0, 0);
    }

    public Direction getRotation(int nbQuarts)
    {
        if (!isValide())return null;// pas de nouvelle direction calculable à partir d'une direction invalide
        {
            nbQuarts = nbQuarts % 4;
            int dirH = getDirH(), dirV = getDirV();
            for (int i = 1; i <= nbQuarts; i++)//rotation nbQuarts de fois d'un quart de tours
            {
                /*
                * (+/-1,x) -> (x,-/+1) : changement de signe et interversion
                * (0,x) -> (x,0) : interversion
                * fonctionne même en diagionale… apparemment
                */
                if (dirH * dirH == 1)dirH = -dirH;
                int aux = dirH;
                dirH = dirV;
                dirV = aux;
            }
            return new Direction(dirH, dirV);
        }
    }
    
    @Override
    public String toString()
    {
        if(!isValide()) return "direction invalide";
        else
        {
            String dirHS="",dirVS="";
            switch(getDirH())
            {
                case -1:dirHS="gauche";break;
                case  0:dirHS="";break;
                case  1:dirHS="droite";
            }
            switch(getDirV())
            {
                case -1:dirVS="haut";break;
                case  0:dirVS="";break;
                case  1:dirVS="bas";
            }
            if(!dirHS.equals(""))dirVS="-"+dirVS;
            return dirHS+dirVS;
        }
    }
}
