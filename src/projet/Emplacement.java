/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

/**
 *
 * @author jo
 */
public class Emplacement
{

    private CoupleEntiers couple;

    public int getColonne(){return couple.getI();}

    public void setColonne(int dirH){couple.setI(dirH);}

    int getLigne(){return couple.getJ();}

    public void setLigne(int dirV){couple.setI(dirV);}

    public boolean isDans()
    {
        Jeu j=Jeu.getCourant();
        if (j == null) return false;
        else
        {
            int nCol = getColonne(), nLig = getLigne(), dim = j.getDim();
            return (nCol >= 1 && nCol <= dim) && (nLig >= 1 && nLig <= dim);
        }
    }
    public boolean isAuBord()
    {
        Jeu j=Jeu.getCourant();
        if (j == null) return false;
        else
        {
            int nCol = getColonne(), nLig = getLigne(), dim = j.getDim();
            return (nCol == 1) || (nCol == dim) || (nLig == 1) || (nLig == dim);
        }        
    }
    
    public Direction getDir()// direction à prendre à partir d'un bord
    {
        Jeu j=Jeu.getCourant();
        System.out.println("Direction.getDir() => (colonne,ligne) = "+getColonne()+","+getLigne());
        if(!isAuBord()) return null;
        else
        {
            int dirX=0,dirY=0,max = j.getDim();
            if(getColonne()==1){dirX=1;dirY=0;}
            else if(getColonne()==max){dirX=-1;dirY=0;}
            else if(getLigne()==1){dirX=0;dirY=-1;}
            else if(getLigne()==max){dirX=0;dirY=1;}
            return new Direction(dirX,dirY);
        }
    }
    
    public Emplacement(CoupleEntiers c)
    {

        setCoupleEntiers(c);
    }

    private void setCoupleEntiers(CoupleEntiers c) {
        this.couple = c;
        //this.setColonne(c.getJ());
        //this.setLigne(c.getI());
    }

    public Emplacement(int col, int lig)
    {
        this(new CoupleEntiers(col, lig));
    }

    public Emplacement(Emplacement e)
    {
        this(new CoupleEntiers(e.getColonne(), e.getLigne()));
    }
    
    public Emplacement()
    {
        Random r = new Random();
        int dim =Jeu.getCourant().getDim();
        //System.out.println("Emplacement() + "+ dim+","+r.nextInt());
        this.couple = new CoupleEntiers(r.nextInt(dim)+1,r.nextInt(dim)+1);
    }
    
    public Emplacement getSuivant(Direction d)
    {
        Emplacement e = new Emplacement(getColonne() + d.getDirH(), getLigne() + d.getDirV());
        return e;

    }
    public boolean equals(Object o){
        if(o == null) return false;
        if(this == o) return true;
        if(o instanceof  Emplacement){
            Emplacement e = (Emplacement) o;
            return e.getCoupleEntiers().equals(getCoupleEntiers());
        }
        else return false;
    }
    public CoupleEntiers getCoupleEntiers(){
        return couple;
    }
    @Override
    public String toString()
    {
        return "case(" + this.getColonne() + "," + this.getLigne() + ")";
    }
}
