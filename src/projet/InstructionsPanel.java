package projet;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class InstructionsPanel extends JPanel {
    private static InstructionsPanel instance;
    private static int W= 400,H=400;
    boolean showing=false;
    public void setVisible(boolean show){
        super.setVisible(show);
        showing = show;
    }
    public static InstructionsPanel getInstance(){
        if(instance==null){
            instance = new InstructionsPanel();
        }
        return  instance;
    }
    public static void invoke(){
        if(!getInstance().showing){
            getInstance().setBounds(Jeu.getCourant().getWidth() / 2 - W / 2, Jeu.getCourant().getHeight() / 2 - H / 2,
                    W, H);
            getInstance().setVisible(true);

        }
        else {
            getInstance().setVisible(false);
        }
    }
    public InstructionsPanel(){
        setLayout(new BorderLayout());
        Color bg = new Color(255,255,255,255);
        setBorder(BorderFactory.createLineBorder(Color.gray,3,true));
        setBackground(bg);
        String text="<html>";
        text+="<body style='padding:10px;text-align:justify;  text-justify: inter-word;'><center><h1>Instructions</h1></center><br/>";
        text+="Cliquez sur les bords pour lancer la particule <br/>";
        text+="Cliquez sur chaque cellule pour donner un hypothese et pui confirmer<br/>";
        text+="&uarr; clé: Incrementer le poids de particule à lancer <br/>";
        text+="&darr clé: Decrementer le poids de particule à lancer ";
        text+="</body><html>";

        Font font = new Font("Monospaced",Font.PLAIN,20);
        JLabel lbl =new JLabel(text);
        lbl.setVerticalAlignment(JLabel.TOP);
        lbl.setFont(font);

        JButton button = new JButton("Fermer");
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                InstructionsPanel.invoke();
            }
        });

        JPanel bottomPane = new JPanel();
        bottomPane.setLayout(new GridLayout(2,3));
        bottomPane.add(new JLabel(""));
        bottomPane.add(button);
        bottomPane.setBackground(bg);
        button.setBackground(Jeu.getCourant().centerPane.markerColor);

        bottomPane.add(button);
        bottomPane.add(new JLabel(""));
        bottomPane.add(new JLabel(""));
        bottomPane.add(new JLabel(""));
        bottomPane.add(new JLabel(""));

        add(lbl,BorderLayout.CENTER);
        add(bottomPane,BorderLayout.PAGE_END);
        setSize(new Dimension(W,H));
        setVisible(false);
        //setOpaque(true);

    }
}
