package projet;


/**
 * Décrivez votre classe Teleporteur ici.
 *
 * @author (votre nom)
 * @version (un numéro de version ou une date)
 */
public class Teleporteur extends Obstacle
{
   private Emplacement destination;
   
   public void execute(Particule p)
   {
       p.setPosition(getDestination());
   }

    public Emplacement getDestination()
    {
        return this.destination;
    }

    public void setDestination(Emplacement destination)
    {
        this.destination = destination;
    }
    public Teleporteur(int poids, Emplacement destination)
    {
        super(poids);
        setDestination(destination);
        setType("Téléporteur");
    }
    public Teleporteur()
    {
        this(Jeu.getCourant().getAleaPoidsObstacle(),new Emplacement());
    }
    public String toString(){
       return  getType() + " vers "+destination + " de poids " + getPoids();
    }
}
