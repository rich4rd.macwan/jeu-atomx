package projet;



import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.Box.Filler;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Décrivez votre classe Jeu ici.
 *
 * @author (votre nom)
 * @version (un numéro de version ou une date)
 */
public class Jeu extends JFrame implements  KeyListener
{
    //UI elements
    JPanel leftPane,rightPane,topPane,bottomPane;
    JLabel lblStatus, lblScore;
    PlateauPane centerPane;
    BorderLayout layout;
    Filler topLeftPadding,topRightPadding,leftTopPadding,leftBottomPadding,rightTopPadding;
    Dimension paddingDim;
    Font font;
    int mouseX, mouseY;
    //MouseMotion mouselistener;
    boolean running;
    private static Jeu jeuCourant;
    private static final int WIDTH = 800;
    private static final int HEIGHT = 800;
    private BufferedImage logo;

    public static Jeu getCourant(){return Jeu.jeuCourant;}
    
    private LesJoueurs joueurs;
    public LesJoueurs getJoueurs(){return this.joueurs;}
    public void setJoueurs(int nbJoueurs)
    {
        joueurs=new LesJoueurs(nbJoueurs);
    }
    public void setJoueurCourant(){joueurs.setRangJoueurCourant();}
    public Joueur getJoueurCourant(){return this.joueurs.getJoueurCourant();}
    public void setJoueurSuivant(){this.joueurs.incRangJoueurCourant();}
    HashMap<CoupleEntiers,Obstacle> obstaclePositions;

    private Obstacle[][] plateau;
    public void setPlateau()
    {
        int dim=this.getDim();
        plateau = new Obstacle[dim][dim];
        obstaclePositions = new HashMap<>();

        for(int i=1;i<=getNbObstacles();i++)
        {
            Emplacement e;
            Obstacle o = Obstacle.getInstance();
            boolean getnew = false;
            do {
                e = new Emplacement();
                //Verify that teleporters do not teleport on the same column or row
                if(o instanceof  Teleporteur){
                    Emplacement dest = ((Teleporteur)o).getDestination();
                    while(dest.getLigne() == e.getLigne() || dest.getColonne() == e.getColonne() ){
                        System.out.println("Getting new emplacement for obstacle: "+ o+ " not good at " + e) ;
                        e = new Emplacement();
                        ((Teleporteur) o).setDestination(e);
                    }
                }
            } while(getPlateau(e)!=null);

            //System.out.println("setPlateau put "+o+" at " + e.getCoupleEntiers());
            setPlateau(o,e);
        }

    }

    public void revealObstacles(){
        centerPane.reveal(obstaclePositions);
    }
    private int maxPoidsObstacle;
    private int nbObstacles;
    private int nbJoueurs;
    private int creditInitial;
    private int dimPlateau;
    
    public int getDim(){return dimPlateau;}
    private void setDim(int dimPlateau){this.dimPlateau=dimPlateau;}
    
    public Obstacle getPlateau(Emplacement e)
    {
        if(!e.isDans())return null;
        else return this.plateau[e.getColonne()-1][e.getLigne()-1];
    }
    public void setPlateau(Obstacle o,Emplacement e)
    {
        if(e.isDans()) {
            this.plateau[e.getColonne() - 1][e.getLigne() - 1] = o;
            obstaclePositions.put(e.getCoupleEntiers(),o);
        }
    }
       
    private Particule particuleCourante;

    public Particule getParticuleCourante()
    {
        return this.particuleCourante;
    }

    public void setParticuleCourante(Particule particuleCourante)
    {
        this.particuleCourante = particuleCourante;
    }

    public int getMaxPoidsObstacle()
    {
        return this.maxPoidsObstacle;
    }
    public int getAleaPoidsObstacle()
    {
        return (int)(ThreadLocalRandom.current().nextInt(1, maxPoidsObstacle));
    }

    public void setMaxPoidsObstacle(int maxPoidsObstacle)
    {
        this.maxPoidsObstacle = maxPoidsObstacle;
    }

    public int getNbObstacles()
    {
        return this.nbObstacles;
    }

    public void setNbObstacles(int nbObstacles)
    {
        this.nbObstacles = nbObstacles;
    }
    
    public void decNbObstacles(){this.nbObstacles--;}

    public int getCreditInitial()
    {
        return this.creditInitial;
    }

    public void setCreditInitial(int creditInitial)
    {
        this.creditInitial = creditInitial;
    }

    public void startExecuteOnce(Particule courante){
        Joueur joueurCourant=this.getJoueurCourant();
        //Particule courante = joueurCourant.getParticuleLancee();
        joueurCourant.decCredit(courante.getPoids());
        Jeu.updateScore();
    }
    public void executeOnce(Particule courante){
        Joueur joueurCourant=this.getJoueurCourant();
        //Particule courante = joueurCourant.getParticuleLancee();
        //joueurCourant.decCredit(courante.getPoids());

        //System.out.println("Credit apres lancer:"+ joueurCourant.getCredit());

        this.setParticuleCourante(courante);
        //System.out.println("Jeu.executeOnce) => "+courante.getPosition()+ " | "+courante.getDirection());
        Emplacement positionAnterieure;

        do
        {
            positionAnterieure=courante.getPosition();
            Obstacle o = this.getPlateau(courante.getPosition());

            if(o!=null)o.agitSur(courante);
            courante.avance();
        }while(courante.isActive());


        //System.out.println("End :"+ courante.getPosition());

        if(courante.getPosition().isDans())
        {
            System.out.println("La particule n'est pas ressortie.");
        }
        else
        {
            System.out.println("Jeu.executeOnce => Une particule de poids "+courante.getPoids()+" est ressortie par la case "+positionAnterieure);
            joueurCourant.incCredit(courante.getPoids());
            centerPane.animateCell(courante.getPosition(), GridButton.ANIMTYPE_ATTRAPER);
            Jeu.updateScore();
            System.out.println(joueurCourant);
        }
        //Perform animation
        //System.out.println(joueurCourant);
        if(joueurCourant.getCredit()<=0){
            System.out.println("Game Over!");
            //setEnabled(false);
            GameEnd.invoke(false);
        }


    }
    public void execute()
    {
        setPlateau();
        this.setJoueurCourant();
        Joueur joueurCourant=this.getJoueurCourant();
        Jeu.jeuCourant=this;
        while(this.getNbObstacles()>0)
        {
            //TODO: Lance particle in UI by clicks, inc/dec weight using W/S keys
            Particule courante = joueurCourant.getParticuleLancee();
            joueurCourant.decCredit(courante.getPoids());
            this.setParticuleCourante(courante);

            Emplacement positionAnterieure;
            do
            {
                positionAnterieure=courante.getPosition();
                courante.avance();
                Obstacle o = this.getPlateau(courante.getPosition());
                if(o!=null)o.agitSur(courante);
            }while(courante.isActive());
            if(courante.getPosition().isDans())
            {
                System.out.println("La particule n'est pas ressortie.");
            }
            else
            {
                System.out.println("Une particule de poids "+courante.getPoids()+" est ressortie par la case "+positionAnterieure);
                joueurCourant.incCredit(courante.getPoids());
                System.out.println(joueurCourant);
            }


        }
        Joueur gagnant=this.getJoueurs().getGagnant();
        if(gagnant!=null)
        {
            System.out.println("Gagnant : "+gagnant);
        }
    }

    public Jeu(int dimPlateau, int nbObstacles, int maxPoidsObstacle, int nbJoueurs, int creditInitial)
    {
        super();
        Jeu.jeuCourant=this;
        init(dimPlateau, nbObstacles, maxPoidsObstacle,nbJoueurs,creditInitial);
    }

    private void init(int dimPlateau, int nbObstacles, int maxPoidsObstacle, int nbJoueurs, int creditInitial) {
        if(centerPane!=null) getContentPane().removeAll();

        setDim(dimPlateau);
        setNbObstacles(nbObstacles);
        setMaxPoidsObstacle(maxPoidsObstacle);
        setCreditInitial(creditInitial);
        setJoueurs(nbJoueurs);
        setupUI();
        setPlateau();
        updateScore();
        //revealObstacles();
    }

    public static void setMessage(String text){

       Jeu.getCourant().lblStatus.setText(text);
    }

    public static void setScoreText(String scoreText){
        Jeu.getCourant().lblScore.setText(scoreText);
    }

    public static void updateScore(){
        if(jeuCourant!=null && jeuCourant.getJoueurCourant()!= null) {
            String pre = jeuCourant.getJoueurCourant().getCredit()<10?" ":"";
            setScoreText(" Score : " + pre +jeuCourant.getJoueurCourant().getCredit() + " ");
        }
    }
    private void setupUI() {

        layout = new BorderLayout(5,5);
        JLayeredPane lpane = new JLayeredPane();
        JPanel mainPanel = new JPanel();
        mainPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

        int dim = getDim();
        mainPanel.setLayout(layout);
        //Add labels for index positions to the left and right
        leftPane = new JPanel(){
            int aDim = 20;
            public void paintComponent(Graphics g){
                super.paintComponent(g);
                int pad = leftTopPadding.getHeight();
                g.setColor(Color.GRAY);
                //g.fillRect(0,mouseY,5,5);
                int[] xPoints = {0,0,aDim};
                int[] yPoints = {mouseY-aDim/2-pad,mouseY+aDim/2-pad,mouseY-pad};
                g.fillPolygon(xPoints,yPoints,3);
                //repaint();
            }
        };

        leftPane.setLayout(new BoxLayout(leftPane,BoxLayout.Y_AXIS));
        rightPane = new JPanel();
        rightPane.setLayout(new BoxLayout(rightPane,BoxLayout.Y_AXIS));

        try {
            logo = (ImageIO.read(getClass().getClassLoader().getResource("resources/logo.png")));
        } catch (IOException e) {
            e.printStackTrace();
        }
        //topPane = new JPanel();
        /*JLabel lbl = new JLabel("");
        lbl.setIcon(new ImageIcon(logo));
        topPane.add(lbl);
*/
        //leftPane.add(new JLabel("Hello"));

        centerPane = new PlateauPane(this);
        centerPane.setLayout(new GridLayout(dim+2,dim+2));


        bottomPane = new JPanel();
        lblStatus = new JLabel(" "  );
        System.setProperty("awt.useSystemAAFontSettings","on");

        Font f = new Font("Helvetica",Font.PLAIN,16);
        lblStatus.setFont(f);
        lblScore = new JLabel(" Credit : ____ ");
        lblScore.setFont(f);
        lblScore.setHorizontalAlignment(JLabel.RIGHT);
        bottomPane.setLayout(new BoxLayout(bottomPane,BoxLayout.X_AXIS));
        JButton helpButton = new JButton("Instructions");
        helpButton.setBackground(centerPane.markerColor);
        helpButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                InstructionsPanel.invoke();
            }
        });

        bottomPane.add(lblScore);
        bottomPane.add(Box.createRigidArea(new Dimension(250,0)));
        bottomPane.add(helpButton);
        bottomPane.add(Box.createHorizontalGlue());
        bottomPane.add(lblStatus);
        lblStatus.setHorizontalAlignment(JLabel.RIGHT);




        mainPanel.add(leftPane,BorderLayout.LINE_START);
        mainPanel.add(rightPane,BorderLayout.LINE_END);
        //mainPanel.add(topPane,BorderLayout.PAGE_START);
        mainPanel.add(bottomPane,BorderLayout.SOUTH);
        mainPanel.add(centerPane,BorderLayout.CENTER);
        mainPanel.setBounds(0,0,WIDTH,HEIGHT);
       // setPreferredSize(new Dimension(WIDTH+100,HEIGHT+100));


        //mainPanel.setBorder(BorderFactory.createLineBorder(Color.red));
        lpane.add(mainPanel, new Integer(0));

        /*JPanel test = new JPanel();
        test.add(new JButton("Hello"));
        test.setBounds(50,50,300,300);
        test.setBackground(new Color(255,255,255,0));*/

        lpane.add(ConfirmActionDialog.getInstance(),new Integer(1));
        lpane.add(InstructionsPanel.getInstance(),new Integer(2));
        lpane.add(GameEnd.getInstance(),new Integer(2));

        lpane.setBounds(0,0,WIDTH,HEIGHT);
        centerPane.initPlateau(dim);
        lpane.setPreferredSize(new Dimension(WIDTH,HEIGHT));

        JComponent newContentPane = new JPanel();
        newContentPane.setOpaque(true); //content panes must be opaque
        setContentPane(newContentPane);
        newContentPane.add(lpane);



        pack();
        setResizable(false);
        setVisible(true);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        jeuCourant = this;
        setTitle("AtomX");
        try {
            BufferedImage icon = (ImageIO.read(getClass().getClassLoader().getResource("resources/atom.png")));
            setIconImage(icon);
        } catch (IOException e) {
            e.printStackTrace();
        }

        running = true;
        for(KeyListener list:getKeyListeners()){
            removeKeyListener(list);
        }
        addKeyListener(this);
        setFocusable(true);
        requestFocus();
        setFocusTraversalKeysEnabled(false);


    }

public static Jeu newGame(){
        if(Jeu.getCourant()!=null) {
           Jeu.getCourant().init(10, 4, 10, 1, 40);
           Jeu.getCourant().repaint();
           return Jeu.getCourant();
        }else{
            return new Jeu(10,4,10,1,40);
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if(e.getKeyCode()==KeyEvent.VK_UP){
            //System.out.println("Increment poids de particule a lancer");
            int val = getJoueurCourant().getPoidsDeParticuleALancer();
            System.out.println("Jeu.keyPressed() : "+ val);
            val = val<getJoueurCourant().getCredit()?val+1:getJoueurCourant().getCredit();
            getJoueurCourant().setPoidsDeParticuleALancer(val);
            centerPane.repaintDynamics();
        }
        if(e.getKeyCode()==KeyEvent.VK_DOWN){
            //System.out.println("Decrement poids de particule a lancer");
            int val = getJoueurCourant().getPoidsDeParticuleALancer();
            val = val>0?val-1:0;
            getJoueurCourant().setPoidsDeParticuleALancer(val);
            centerPane.repaintDynamics();
        }

        //System.out.println("Shift down ?" + e.isShiftDown());
        //System.out.println("Alt down ?" + e.isAltDown());
        //System.out.println("Ctrl down ?" + e.isControlDown());
        if(e.isControlDown() && e.isAltDown() && e.getKeyCode() == KeyEvent.VK_R){
            //Hack for revealing all obstacles
            System.out.println("You sly bastard!");
            Jeu.getCourant().revealObstacles();
        }


    }

    @Override
    public void keyReleased(KeyEvent e) {

    }
}
