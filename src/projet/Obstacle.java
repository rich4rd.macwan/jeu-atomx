package projet;


import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Décrivez votre classe Obstacle ici.
 *
 * @author (votre nom)
 * @version (un numéro de version ou une date)
 */
public abstract class Obstacle extends Pondere
{    
    public static Obstacle getInstance()
    {
        if(listeTypes == null)
            setListeTypes();
        return getInstance(ThreadLocalRandom.current().nextInt(1, Obstacle.getNbTypes() + 1));
        //return getInstance((int)(Math.random()*Obstacle.getNbTypes()));
    }
    private String type;
    public static Obstacle getInstance(int rangType)
    {
        //System.out.println("getInstance rangType= "+rangType);
        Obstacle o;
        switch(rangType)
        {
            case 1:o=new Prison();break;
            case 2:o=new Deviateur();break;
            case 3:o=new Teleporteur();break;
            default : o=null;
        }
        return o;
    }
    private static java.util.ArrayList<String> listeTypes;
    public static void setListeTypes()
    {
        listeTypes = new ArrayList<>();
//        listeTypes.add("Prison");
//        listeTypes.add("Déviateur");
//        listeTypes.add("Téléporteur");
        int i=1;Obstacle o;
        do
        {
            o=getInstance(i);
            if(o!=null) {
                //System.out.println(o);
                listeTypes.add(o.getType());
            }
            i++;
        }while(o!=null);
    }
    public static java.util.ArrayList<String> getListeTypes(){return Obstacle.listeTypes;}
    public static String getType(int i){return Obstacle.getListeTypes().get(i-1);}
    
    public void agitSur(Particule a)
    {
        //System.out.println("particule.poids >= obstacle.poids? "+ a.getPoids()+" >= "+getPoids());
        if(a.getPoids()>=this.getPoids())execute(a);
    }
    public static int getNbTypes(){
        return listeTypes.size();
    }

    public String getType(){return this.type;};
    public void setType(String s){this.type=s;}
    
    public abstract void execute(Particule a);
    
    public Obstacle(int poids)
    {
        super(poids);
    }

    public abstract String toString();
    //{
    //    return "Obstacle "+getType()+" de poids "+this.getPoids();
    //}
}
