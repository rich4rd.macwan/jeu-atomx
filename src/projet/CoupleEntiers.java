package projet;


/**
 * Décrivez votre classe Emplacement ici.
 *
 * @author (votre nom)
 * @version (un numéro de version ou une date)
 */
public class CoupleEntiers extends Object
{
   private int i,j;

    public int getI()
    {
        return i;
    }

    public void setI(int i)
    {
        this.i = i;
    }

    public int getJ()
    {
        return j;
    }

    public void setJ(int j)
    {
        this.j = j;
    }

    public CoupleEntiers(int i, int j)
    {
        setI(i);
        setJ(j);
    }

    @Override
    public String toString()
    {
        return "("+getI()+","+getJ()+")";
    }

    @Override
    public boolean equals(Object o){
        if(o == null) return false;
        if(this == o) return true;
        if(o instanceof  CoupleEntiers){
            CoupleEntiers c = (CoupleEntiers)o;
            return getI()==c.getI() && getJ()==c.getJ();
        }
        else return false;
    }
    @Override
    public int hashCode() {
        int result = 0;
        result = (int) (getI()*20+getJ());
        return result;
    }
}
