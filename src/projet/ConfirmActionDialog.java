package projet;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.Border;
import javax.tools.Tool;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.awt.image.FilteredImageSource;
import java.awt.image.ImageProducer;
import java.awt.image.RGBImageFilter;
import java.io.IOException;
import java.nio.Buffer;

import static com.sun.java.accessibility.util.AWTEventMonitor.addActionListener;

public class ConfirmActionDialog extends JPanel {
    private static ConfirmActionDialog instance;
    private static int HEIGHT = 28;
    private static int WIDTH = 76;
    private static int shadowOffset = 1;
    private static JPanel panel;
    private static ToolbarButton tickButton,closeButton;
    private static GridButton clickSource;
    public  static Color inactiveColor = new Color(255,255,255,0);
    private  static Color activeColor = new Color(220,230,255,255);
    //private ConfirmActionDialog(Jeu jeu){        super(jeu);    }
    public static ConfirmActionDialog getInstance(){
        if(instance==null){
            //instance = new ConfirmActionDialog(Jeu.getCourant());
            instance = new ConfirmActionDialog();

            //panel = new JPanel();
            //panel.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY));
            //instance.getContentPane().add(panel);
            //instance.setSize(WIDTH,HEIGHT);
            //instance.setBackground(inactiveColor);
            //panel.setBackground(new Color(250,250,250,180));

            instance.setBackground(inactiveColor);
            /*instance.getContentPane().setLayout(new BoxLayout(instance.getContentPane(),BoxLayout.X_AXIS));
            instance.getContentPane().add(new ToolbarButton(0));
            instance.getContentPane().add(new ToolbarButton(-1));
            instance.getContentPane().add(new ToolbarButton(1));
            instance.pack();*/
            instance.setLayout(new BoxLayout(instance,BoxLayout.X_AXIS));
            tickButton = new ToolbarButton(0);
            instance.add(tickButton);

            instance.add(new ToolbarButton(-1));
            closeButton = new ToolbarButton(1);
            instance.add(closeButton);
            instance.setOpaque(false);
           // instance.setBounds(0,0,100,200);
        }
        return instance;
    }
    public static void invoke(GridButton source, int x, int y, int parentW){
        clickSource = source;
        getInstance().setBounds(x+parentW/8,y-parentW,WIDTH,HEIGHT);
        getInstance().setVisible(true);

    }

    public static void revoke(){
        getInstance().setVisible(false);
    }


    static class ToolbarButton extends JComponent implements MouseListener {
        int imgType = 0;
        BufferedImage icon = null;
        Image shadow1 = null, shadow2 = null;
        boolean hover = false;
        Border border = BorderFactory.createLineBorder(Color.gray);

        public void paint(Graphics g) {
            super.paintComponent(g);

            //System.out.println("Am i being called? ");
            //g.fillOval(0,0,30,30);
            //g.clearRect(0,0,ConfirmActionDialog.WIDTH,ConfirmActionDialog.HEIGHT);
            if (imgType != -1) {
                g.drawImage(shadow1, shadowOffset, 0, icon.getWidth() + shadowOffset
                        , icon.getHeight() + shadowOffset, null);
                if (hover) {
                    g.drawImage(shadow2, shadowOffset, 0, icon.getWidth() + shadowOffset
                            , icon.getHeight() + shadowOffset, null);

                }
                g.drawImage(icon, 0, 0, null);

            }

        }

        public ToolbarButton(int type) {
            imgType = type;
            try {
                switch (type) {
                    case 0:
                        //Confirm button
                        icon = ImageIO.read(instance.getClass().getClassLoader().getResource("resources/confirm.png"));
                        break;
                    case 1:
                        //Cancel button
                        icon = ImageIO.read(instance.getClass().getClassLoader().getResource("resources/cancel.png"));
                        break;
                    default:
                        //icon = ImageIO.read(instance.getClass().getClassLoader().getResource("resources/cancel.png"));
                        break;

                }
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
            if (icon != null) {

                // a filter which converts all colors except 0 to black
                RGBImageFilter filter = new RGBImageFilter() {
                    @Override
                    public int filterRGB(int x, int y, int rgb) {
                        if (rgb == 0)
                            return 0;
                        else
                            return 0x66000000;
                    }
                };


                ImageProducer prod = new FilteredImageSource(icon.getSource(), filter);

                // create whe black image
                shadow1 = Toolkit.getDefaultToolkit().createImage(prod);
                shadow2 = Toolkit.getDefaultToolkit().createImage(prod);


                setSize(icon.getWidth(), icon.getHeight());
            }

            addMouseListener(this);

            setDoubleBuffered(true);
        }

        @Override
        public void mouseClicked(MouseEvent e) {

            if (e.getSource() instanceof ToolbarButton && ((ToolbarButton) e.getSource()) != closeButton) {
                if(clickSource.validateObstacle(clickSource.getEmplacement())){
                    clickSource.setBackground(PlateauPane.markerColor);
                }else{
                    clickSource.hideObstacle();
                }
            }else {
                clickSource.hideObstacle();
            }
            clickSource.repaint();
            ConfirmActionDialog.getInstance().revoke();
        }

        @Override
        public void mousePressed(MouseEvent e) {

        }

        @Override
        public void mouseReleased(MouseEvent e) {

        }

        @Override
        public void mouseEntered(MouseEvent e) {
            if (imgType >= 0) {
                hover = true;
                repaint();
            }
        }

        @Override
        public void mouseExited(MouseEvent e) {
            hover = false;
            repaint();
        }

    }
}
