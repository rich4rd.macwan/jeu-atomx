/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet;

/**
 *
 * @author jo
 */
public class Particule extends Pondere
{

    private boolean active;
    public boolean isActive()
    {
        return this.active;
    }
    public void setActive(boolean active)
    {
        this.active = active;
    }
    
    private Direction direction;
    public Direction getDirection()
    {
        return this.direction;
    }
    public void setDirection(Direction direction)
    {
        this.direction = direction;
    }
    
    private Emplacement position;
    public Emplacement getPosition()
    {
        return this.position;
    }
    public void setPosition(Emplacement e)
    {
        this.position = e;
    }
    
    public Particule(Emplacement e,Direction d,int poids)
    {
        super(poids);
        this.setPosition(e);
        this.setDirection(d);
        this.setActive(true);
    }

    public void avance()
    {
        if (isActive())// pas d'avancée si la particule est inactive
        {
            //System.out.println("Particule.avance() => getPostion() = "+ getPosition() +", dir : "+getDirection());

            //System.out.println("Particule.avance() => getDirection() = "+ getDirection());
            Emplacement e = getPosition().getSuivant(getDirection());
            setPosition(e);
            setActive(e.isDans());// inactif si sortie du plateau
        }
    }

    public void tourne(int nbQuarts)
    {
       if (isActive())// pas de rotation si la particule est inactive
         setDirection(getDirection().getRotation(nbQuarts));
    }
    
    public String toString()
    {
        String etat;
        if(isActive())etat="actif";
        else etat="inactif";
        return
                "Position : "+getPosition()+"\n"+
                "Direction : "+getDirection()+"\n"+
                "Poids : "+getPoids()+"\n"+
                "État : "+etat;
    }
}
