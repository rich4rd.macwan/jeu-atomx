/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet;

/**
 *
 * @author jo
 */
public class LesJoueurs
{
    private Joueur[] joueurs;
    public int getNbJoueurs()
    {
        return this.joueurs.length;
    }
    public void setJoueurs(int nbJoueurs)
    {
        if(nbJoueurs<=0)
        {
            System.out.println("Nombre de joueurs invalide. Il est ramené à 1");
            nbJoueurs=1;
        }
        if(nbJoueurs>=10)
        {
            nbJoueurs=10;
            System.out.println("Nombre de joueurs invalide. Il est ramené à 10");
        }
        joueurs = new Joueur[nbJoueurs];
        for(int i=0;i<nbJoueurs;i++) {
            joueurs[i]=new Joueur();
            //System.out.println("Set initial credit = "+Jeu.getCourant().getCreditInitial());
            joueurs[i].setCredit(Jeu.getCourant().getCreditInitial());
            joueurs[i].setPoidsDeParticuleALancer(Jeu.getCourant().getMaxPoidsObstacle());
        }
    }
    public Joueur getJoueur(int i)
    {
        if(i<0||i>=this.getNbJoueurs())return null;
        else return this.joueurs[i];
    }
    
    public Joueur getGagnant()
    {
        Joueur gagnant=this.getJoueur(0);
        for(int i=1;i<this.getNbJoueurs();i++)
            if(this.getJoueur(i).getCredit()>gagnant.getCredit())
                gagnant=this.getJoueur(i);
        return gagnant;
    }
    private int rangJoueurCourant;
    public void setRangJoueurCourant()
    {
        this.rangJoueurCourant=(int)(Math.random()*this.getNbJoueurs()+1);
    }
    public void incRangJoueurCourant()
    {
        this.rangJoueurCourant++;
        if(this.rangJoueurCourant==this.getNbJoueurs())this.rangJoueurCourant=0;
    }
    private int getRangJoueurCourant(){return this.rangJoueurCourant;}
    public Joueur getJoueurCourant()
    {
        return joueurs[this.getRangJoueurCourant()];
    };
    
    public LesJoueurs(int nbJoueurs)
    {
        setJoueurs(nbJoueurs);
    }

}
