package projet;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

public class GameEnd extends JPanel {
    private static GameEnd instance;
    private static int W= 800,H=800;
    private static ImageIcon lost,won;
    private static JLabel lbl;
    boolean showing=false;
    public void setVisible(boolean show){
        super.setVisible(show);
        showing = show;
    }

    public static GameEnd getInstance(){
        if(instance==null){
            instance = new GameEnd();

        }
        return  instance;
    }
    public static void invoke(boolean win){
        if(!getInstance().showing){
            getInstance().setBounds( Jeu.getCourant().getWidth() / 2 - W / 2, Jeu.getCourant().getHeight() / 2 - H / 2,
                    W, H);
            if(win){
                lbl.setIcon(won);
            }else{
                lbl.setIcon(lost);
            }
            getInstance().setVisible(true);
        }
        else {
            getInstance().setVisible(false);
        }


    }
    public GameEnd(){
        setLayout(new BorderLayout());
        Color bg = new Color(255,255,255,0);
        setBackground(bg);
        setBorder(BorderFactory.createEmptyBorder(50, 10, 50, 10));

        lbl =new JLabel("");
        lbl.setVerticalAlignment(JLabel.TOP);
        lbl.setHorizontalAlignment(JLabel.CENTER);
        try {
            lost = new ImageIcon(ImageIO.read(getClass().getClassLoader().getResource("resources/gameover.png")));
            won = new ImageIcon(ImageIO.read(getClass().getClassLoader().getResource("resources/gameoverwin.png")));
            lbl.setIcon(lost);
        } catch (IOException e) {
            e.printStackTrace();
        }
        add(lbl,BorderLayout.CENTER);
        JButton button = new JButton("Restart");

        JPanel bottomPane = new JPanel();
        bottomPane.setBackground(bg);
        bottomPane.setLayout(new GridLayout(1,3));
        bottomPane.add(new JLabel(""));
        bottomPane.add(button);
        button.setBackground(Jeu.getCourant().centerPane.markerColor);

        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Are we here>");
                GameEnd.getInstance().setVisible(false);
                //Jeu.getCourant().setEnabled(true);
                Jeu.newGame();
            }
        });

        bottomPane.add(button);
        bottomPane.add(new JLabel(""));
        add(bottomPane,BorderLayout.PAGE_END);
        setSize(new Dimension(W,H));
        setVisible(false);
    }
}
